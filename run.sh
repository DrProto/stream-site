# change redirect to point at hls server
if [ ! -z "$ADDRESS" ]; then
  sed -i 's/localhost:8080/'"$ADDRESS":8080'/g' /etc/nginx/nginx.conf
  sed -i 's/localhost;/localhost '"$ADDRESS"';/g' /etc/nginx/nginx.conf
  sed -i 's/localhost/'"$ADDRESS"'/g' /usr/share/nginx/html/index.html

  echo "substituting address: " $ADDRESS
fi

echo "Starting server..."
nginx -g "daemon off;"
