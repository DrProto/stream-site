# Stream-site
Docker image for a video js site to watch an RTMP/HLS server. Based on alpine nginx
Designed to watch a HLS playlist from a server such as JasonRivers/nginx-rtmp

## Configuration 
this image exposes port 80 and hosts a simple video js player page. 
the config is found at /etc/nginx/nginx.conf

## Running
To run the container, bind port 80 to the host machine and add an enviromental variable value for ADDRESS. This is where you hls end point is found. The site is designed to point at http://&lt;ADDRESS&gt;/hls/stream.m3u8; 
run the following:
```
docker run -p 80:80 --name stream -e ADDRESS=x.x.x.x registry.gitlab.com/drproto/stream-site
```

